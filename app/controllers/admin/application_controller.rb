class Admin::ApplicationController < ApplicationController
  before_action :check_auth
  private
  def check_auth
    # exercise to the reader to check auth
    Rails.logger.info("CHECKING ADMIN STATUS")
  end
end
