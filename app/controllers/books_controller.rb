class BooksController < ApplicationController
  before_action :set_book, only: [:show]
  before_action :set_author

  # GET /books
  # GET /books.json
  def index
    if @author.present?
      @books = @author.books
    else
      @books = Book.all
    end
  end

  # GET /books/1
  # GET /books/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.find(params[:id])
    end

    def set_author
      # We're using find_by instead of just find here, because find
      # throws an exception if the thing is not found and in this
      # case, we won't always have an author_id, because we still have
      # the top-level book routes
      @author = Author.find_by(id: params[:author_id])
    end

end
