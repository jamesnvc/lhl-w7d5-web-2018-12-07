Rails.application.routes.draw do
  root 'authors#index'

  resources :authors do
    # all this does is make routes exist under authors
    # all the routes still go to the same controllers
    # e.g. /authors/:id/book still calls BooksController index method
    resources :books, only: [:show, :index]
  end

  namespace :admin do
    # this also makes a new route
    # BUT it makes the methods go to a whole new controller
    # Admin::BooksController (which it expects to be in controllers/admin/books_controller.rb)
    resources :books, except: [:show, :index]
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
